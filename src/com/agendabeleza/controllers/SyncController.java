package com.agendabeleza.controllers;

import com.agendabeleza.models.CompanyModel;
import com.agendabeleza.utils.Heroku;
import com.agendabeleza.utils.HerokuClient;
import com.agendabeleza.utils.HerokuException;

public class SyncController {

	public boolean existsOnHeroku(CompanyModel pCompanyModel) {

		HerokuClient client = new HerokuClient(Heroku.HOST + Heroku.COMPANY_SERVICES + pCompanyModel.getAzId());
		CompanyModel tCompany;

		try {
			tCompany = (CompanyModel) client.request(CompanyModel.class);
			if (tCompany.getId() == null) {
				return false;
			} else {
				pCompanyModel.setId(tCompany.getId());
				return true;
			}
		} catch (HerokuException e) {
			System.err.println("resposta nula");
			return false;
		}
	}

	public boolean createOnHeroku(CompanyModel pCompanyModel) {

		HerokuClient client = new HerokuClient(Heroku.HOST + Heroku.COMPANY_SERVICES);
		CompanyModel tCompany;

		try {
			client.setMethod(HerokuClient.POST);
			client.setParamsFromObject(pCompanyModel);
			tCompany = (CompanyModel) client.request(CompanyModel.class);

			if (tCompany.getId() == null) {
				return false;
			} else {
				pCompanyModel.setId(tCompany.getId());
				return true;
			}
		} catch (HerokuException e) {
			System.err.println("resposta nula");
			return false;
		}

	}

}
