package com.agendabeleza.utils;

public class Heroku {

	public static String HOST = "http://127.0.0.1:3000";
	
	public static Integer TIMEOUT = 3000;
	public static String ENCODE = "UTF-8";
	
	public static String COMPANY_SERVICES = "/sync/company/";
	public static String PROFESSIONAL_SERVICES = "/sync/professional/";
	public static String SERVICE_SERVICES = "/sync/service/";
	public static String SKILL_SERVICES = "/sync/skill/";

}
