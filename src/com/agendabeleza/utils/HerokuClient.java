package com.agendabeleza.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

public class HerokuClient {

	public static String GET = "GET";
	public static String PUT = "PUT";
	public static String POST = "POST";
	public static String DELETE = "DELETE";

	private String mUrl;
	private String mMethod = GET;
	private String mParamsString = "";
	private HashMap<String, String> mParams = new HashMap<String, String>();

	public HerokuClient(String pUrl) {
		mUrl = pUrl;
	}

	public void setParam(String pName, Integer pValue) {
		this.setParam(pName, pValue.toString());
	}

	public void setParam(String pName, Object pValue) {

		if (pValue != null) {
			mParams.put(pName, pValue.toString());
			makeParamsString();
		}
	}

	public void setParam(String pName, String pValue) {

		if (pValue != null) {
			mParams.put(pName, pValue);
			makeParamsString();
		}
	}

	public void setParamsFromObject(Object pObject) {

		Class<?> tClass = pObject.getClass();
		Method tMethods[] = tClass.getDeclaredMethods();

		for (int i = 0; i < tMethods.length; i++) {

			Method tMethod = tMethods[i];

			if (tMethod.getName().substring(0, 3).endsWith("get")) {
				try {

					String fieldName = tMethod.getName().substring(3);
					String regex = "([a-z])([A-Z])";
					String replacement = "$1_$2";
					String fieldUnderScorename = fieldName.replaceAll(regex, replacement).toLowerCase();

					Object tValue = tMethod.invoke(pObject);
					
					if( tValue == null )
						tValue = "null";

					setParam(fieldUnderScorename, tValue);

				} catch (IllegalAccessException e) {
					System.err.println(this.getClass() + " acesso ilegal ao objeto " + e);
				} catch (InvocationTargetException e) {
					System.err.println(this.getClass() + " erro na tentativa de invocar o metodo. " + e);
				}
			}
		}

	}

	public void setMethod(String pMethod) {
		mMethod = pMethod;
	}

	public Object request(Class<?> pClass) throws HerokuException {

		HttpURLConnection tConnection;
		URL tUrl;
		String response = null;

		try {

			tUrl = new URL(mUrl + mParamsString);
			tConnection = (HttpURLConnection) tUrl.openConnection();

			if (mMethod == POST) {
				tConnection.setDoOutput(true);
				tConnection.setInstanceFollowRedirects(false);
				tConnection.setRequestMethod(mMethod);
				tConnection.setRequestProperty("Content-Type", "text/plain");
				tConnection.setRequestProperty("Charset", Heroku.ENCODE);
				tConnection.setRequestProperty("Content-Length", "" + Integer.toString(mParamsString.getBytes().length));
				DataOutputStream wr = new DataOutputStream(tConnection.getOutputStream());
				wr.writeBytes(mParamsString);
				wr.flush();
			}

			BufferedReader in = new BufferedReader(new InputStreamReader(tConnection.getInputStream()));
			response = in.readLine();

		} catch (ProtocolException e) {
			System.err.println(this.getClass() + " M�todo do protocolo desconhecido.");
		} catch (MalformedURLException e) {
			System.err.println(this.getClass() + " URL mal formatada.");
		} catch (IOException e) {
			System.err.println(this.getClass() + " Erro de IO. " + e);
		} catch (Exception e) {
			System.err.println(this.getClass());
		}

		if (response != null) {
			Gson gson = new Gson();
			Object object = gson.fromJson(response, pClass);
			return object;
		}

		throw new HerokuException("A resposta do Heroku foi nula!");
	}

	// PRIVATE
	private void makeParamsString() {

		String tParamsString = "";
		if (mParams.size() > 0) {

			tParamsString = "?";
			for (Map.Entry<String, String> entry : mParams.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();

				try {
					tParamsString = tParamsString + key + "=" + URLEncoder.encode(value, Heroku.ENCODE) + "&";
				} catch (UnsupportedEncodingException e) {
					System.err.println(this.getClass() + " encodagem n�o suportada");
				}
			}
		}

		mParamsString = tParamsString;
	}

}
