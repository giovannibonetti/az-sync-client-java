package com.agendabeleza.utils;

public class ApplicationError {
	
	public static Integer CONNECTION_ERROR = 1;
	public static Integer SYNC_ERROR = 2;

}
