package com.agendabeleza.utils;

public class HerokuException extends Exception {
	

	private static final long serialVersionUID = 1L;

	public HerokuException(String message, Throwable cause) {
		super(message, cause);
	}

	public HerokuException(String message) {
		super(message);
	}

	public HerokuException(Throwable cause) {
		super(cause);
	}


}
