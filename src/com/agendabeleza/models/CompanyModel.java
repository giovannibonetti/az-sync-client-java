package com.agendabeleza.models;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyModel {

	@SerializedName("_id")
	private String id;
	private String name;

	private String address;
	@SerializedName("address_number")
	private String addressNumber;
	private String neighborhood;
	private String city;
	private String state;
	private String phone;
	
	@SerializedName("az_calendar_interval")
	private Integer azCalendarInterval;
	@SerializedName("az_id")
	private String azId;
	
	
	@Expose(serialize = false)
	private Boolean sincronized =  false;
	@Expose(serialize = false)
	private Date last_sync;

	public CompanyModel() {
		this.sincronized = false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getAzCalendarInterval() {
		return azCalendarInterval;
	}

	public void setAzCalendarInterval(Integer azCalendarInterval) {
		this.azCalendarInterval = azCalendarInterval;
	}

	public String getAzId() {
		return azId;
	}

	public void setAzId(String azId) {
		this.azId = azId;
	}

	public Boolean getSincronized() {
		return sincronized;
	}

	public void setSincronized(Boolean sincronized) {
		this.sincronized = sincronized;
	}

	public Date getLast_sync() {
		return last_sync;
	}

	public void setLast_sync(Date last_sync) {
		this.last_sync = last_sync;
	}	

}
