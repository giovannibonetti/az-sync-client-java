package com.agendabeleza.providers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * 
 * @author fabiotomio
 *
 */
public class PingHandler implements HttpHandler {
	public void handle( HttpExchange t) throws IOException {
		
		System.out.println("ping");
		
		Headers h = t.getResponseHeaders();
		h.set("Content-Type", "text/; application/json");
		
		HashMap<String, String> temp = new HashMap<String, String>();
		
		temp.put("response", "pong");

		Gson gson = new Gson();
		String response = gson.toJson( temp );
		t.sendResponseHeaders(200, response.length() );
		OutputStream os = t.getResponseBody();
		os.write( response.getBytes() );
		os.close();
	}
}