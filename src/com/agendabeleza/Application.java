package com.agendabeleza;

import java.io.IOException;

import com.agendabeleza.controllers.SyncController;
import com.agendabeleza.models.CompanyModel;

public class Application {

	private static CompanyModel company;
	private static SyncController sync;

	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, InterruptedException {

		sync = new SyncController();

		System.out.println("Agent started");

		// load all az_data
		System.out.println("Loading all AZ DATA");

		company = new CompanyModel();
		company.setAzId("C81E0CAC-90C8-4D14-9B66-F2D8A6DF3083");
		company.setAzCalendarInterval(30);
		company.setName("Empresa teste");
		company.setAddress("Rua: nome da rua");
		company.setAddressNumber("numero");
		company.setNeighborhood("bairro");
		company.setCity("Curitiba");
		company.setState("Parana");
		company.setPhone("(41) 12341234");

		while (true) {
			System.out.println("Verifica��o se a empresa j� est� na nuvem!");
			Boolean exist = sync.existsOnHeroku(company);

			if (!exist) {
				Thread.sleep(4000);
				System.out.println("Empresa n�o existe.");
				System.out.println("Cadastrando a empresa na nuvem!");
				sync.createOnHeroku(company);
			} else {
				System.out.println("Existe =D, c�digo na nuvem: " + company.getAzId());
			}
			Thread.sleep(4000);
		}

		// HttpServer server = HttpServer.create( new InetSocketAddress(8000), 0
		// );
		//
		// server.createContext("/ping", new PingHandler() );
		// server.createContext("/", new HomeHandler() );
		// server.setExecutor(null);
		// server.start();
	}
}