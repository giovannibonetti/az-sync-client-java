# AZ Sync Client - Java #

Este protótipo foi feito em agosto de 2013 para ser instalado nos computadores com AZ e se conectar diretamente no banco de dados Firebird. .

### Tarefas executadas ###

* Conexão inicial ao servidor HTTP. Obtenção de metadados para orientar nos próximos passos (salão novo no sistema? quando foi feita a última atualização?);

* Cada registro da tabela é enviado numa chamada HTTP separada, com um intervalo fixo (provavelmente 1 segundo). O espaçamento grande foi escolhido para dar uma folga suficiente para o servidor processar e salvar cada registro;

* Daí o programa fica rodando em segundo plano para mandar atualizações para o servidor de integração.

### Observações ###

* Neste primeiro protótipo a conexão do computador com AZ era feita diretamente com o servidor Rails no Heroku.